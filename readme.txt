Projet réalisé par Mattéo POIROT  et Flavien LELIEVRE

lien application webetu : https://webetu.iutnc.univ-lorraine.fr/~poirot196u/Tweeter/main.php/home/

lien depot gitlab : https://gitlab.com/matteopoirot/tweeter


url pour afficher les gens qu'on follow : https://webetu.iutnc.univ-lorraine.fr/~poirot196u/Tweeter/main.php/following/

url pour afficher les gens qui nous follow : https://webetu.iutnc.univ-lorraine.fr/~poirot196u/Tweeter/main.php/followers/


compte pour tester les fonctionnalités :

username : test

mot de passe : test
